<?php

/**
 * Returns the session directory if a valid X-Cover-Session-Dir header is present on the request.
 *
 * @return
 *   Either the path to the cover collect session directory or FALSE if the
 *   X-Cover-Session-Dir header does not contain a valid HMAC and timestamp.
 *
 * @see: drupal_generate_valid_ua().
 */
function cover_collect_valid_session_dir_header() {
  global $drupal_hash_salt;
  // No reason to reset this.
  static $test_session_dir;

  if (isset($test_session_dir)) {
    return $test_session_dir;
  }

  if (isset($_SERVER['HTTP_X_COVER_SESSION_DIR'])) {
    list($session_dir, $time, $salt, $hmac) = explode(';', $_SERVER['HTTP_X_COVER_SESSION_DIR']);
    $check_string =  $session_dir . ';' . $time . ';' . $salt;
    // We use the salt from settings.php to make the HMAC key, since
    // the database is not yet initialized and we can't access any Drupal variables.
    // The file properties add more entropy not easily accessible to others.
    $key = $drupal_hash_salt . filectime(__FILE__) . fileinode(__FILE__);
    $time_diff = REQUEST_TIME - $time;
    // Since we are making a local request a 5 second time window is allowed,
    // and the HMAC must match.
    if ($time_diff >= 0 && $time_diff <= 5 && $hmac == drupal_hmac_base64($check_string, $key)) {
      $test_session_dir = $session_dir;
      return $test_session_dir;
    }
  }

  $test_session_dir = FALSE;
  return $test_session_dir;
}

/**
 * Generates an X-Cover-Session-Dir header with a HMAC and timestamp.
 *
 * @see: drupal_generate_test_ua().
 */
function cover_collect_generate_session_dir_header($session_dir) {
  global $drupal_hash_salt;
  static $key;

  if (!isset($key)) {
    // We use the salt from settings.php to make the HMAC key, since
    // the database is not yet initialized and we can't access any Drupal variables.
    // The file properties add more entropy not easily accessible to others.
    $key = $drupal_hash_salt . filectime(__FILE__) . fileinode(__FILE__);
  }
  // Generate a moderately secure HMAC based on the database credentials.
  $salt = uniqid('', TRUE);
  $check_string = $session_dir . ';' . time() . ';' . $salt;
  return 'X-Cover-Session-Dir: ' . $check_string . ';' . drupal_hmac_base64($check_string, $key);
}

/**
 * Return the path to the working directory.
 */
function cover_collect_session_dir_default() {
  if (($session_dir = cover_collect_valid_session_dir_header())) {
    return $session_dir;
  }
  elseif (!empty($_ENV['DRUPAL_COVER_COLLECT_DIR'])) {
    return $_ENV['DRUPAL_COVER_COLLECT_DIR'];
  }
  else {
    return sys_get_temp_dir() . '/drupal-cover-collect-' . substr(md5(__FILE__), 0, 6);
  }
}

/**
 * Switch to another session directory
 */
function cover_collect_session_dir_swap($newdir) {
  static $currentdir;

  if (!isset($currentdir)) {
    $currentdir = cover_collect_session_dir_default();
  }

  if ($newdir !== FALSE && $newdir != $currentdir) {
    cover_collect_recording_stop($currentdir);
    cover_collect_recording_start($newdir);
    $olddir = $currentdir;
    $currentdir = $newdir;
    return $olddir;
  }

  return $currentdir;
}

/**
 * Return current session directory
 */
function cover_collect_session_dir() {
  return cover_collect_session_dir_swap(FALSE);
}

/**
 * Return true if xdebug code coverage is enabled.
 */
function cover_collect_driver_present() {
  return (extension_loaded('xdebug') && ini_get('xdebug.coverage_enable'));
}

/**
 * Return 1 if collection of coverage data is in progress, 0 otherwise.
 */
function cover_collect_recording_status() {
  return cover_collect_recording_control(FALSE);
}

/**
 * Try to start collection of coverage data.
 *
 * Note that collection of code coverage data will not start when either the
 * working directory is not present or xdebug code coverage is not enabled.
 */
function cover_collect_recording_start($session_dir = FALSE) {
  return cover_collect_recording_control(1, $session_dir);
}

/**
 * Stop recording of coverage data.
 */
function cover_collect_recording_stop($session_dir = FALSE) {
  return cover_collect_recording_control(0, $session_dir);
}

/**
 * Try to start / stop collection of code coverage data
 */
function cover_collect_recording_control($desired_status, $session_dir = FALSE) {
  static $status = 0;
  static $file = NULL;

  if ($desired_status !== FALSE && $status != $desired_status && cover_collect_driver_present()) {
    if ($desired_status == 1) {
      if ($session_dir === FALSE) {
        $session_dir = cover_collect_session_dir();
      }

      if (is_dir($session_dir) && is_writable($session_dir)) {
        xdebug_start_code_coverage(XDEBUG_CC_UNUSED | XDEBUG_CC_DEAD_CODE);

        $filename = tempnam($session_dir, 'data-');
        $file = fopen($filename, 'w');

        drupal_register_shutdown_function('cover_collect_recording_stop');
        $status = 1;
      }
    }
    elseif ($desired_status == 0) {
      $data = xdebug_get_code_coverage();
      xdebug_stop_code_coverage();

      fwrite($file, serialize($data));
      fclose($file);
      $file = NULL;

      $status = 0;
    }
  }

  return $status;
}
