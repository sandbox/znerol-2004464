<?php
if (!empty($drupal_hash_salt)) {
  require_once __DIR__ . '/cover.collect.inc';
  cover_collect_recording_start();

  if (!defined('COVER_BOOTSTRAP_INC')) {
    define('COVER_BOOTSTRAP_INC', TRUE);
  }
}
elseif (!defined('COVER_BOOTSTRAP_INC')) {
  define('COVER_BOOTSTRAP_INC', FALSE);
}
