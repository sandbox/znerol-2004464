<?php

/**
 * Generate code coverage report
 *
 * FIXME: Probably move that into a batch
 */
function cover_simpletest_generate($test_id = NULL) {
  if (!$test_id) {
    return;
  }

  require_once 'PHP/CodeCoverage/Autoload.php';
  $source = 'public://simpletest/cover/raw/' . $test_id;
  $dest = 'public://simpletest/cover/report/' . $test_id;

  if (!is_dir($source)) {
    // FIXME: show sensible error message and redirect back to results
    drupal_not_found();
  }

  if (!file_prepare_directory($dest, FILE_CREATE_DIRECTORY)) {
    // FIXME: show sensible error message and redirect back to results
    drupal_not_found();
  }

  // Register simpletest PSR-0 test case autoloader.
  if (function_exists('_simpletest_autoload_psr0')) {
    spl_autoload_register('_simpletest_autoload_psr0');
  }

  // Construct top-level coverage class
  $allcover = new PHP_CodeCoverage();
  $allcover->setProcessUncoveredFilesFromWhitelist(TRUE);

  // Scan directory structure for results
  $results = cover_simpletest_scan_results($source);

  foreach ($results as $class => $methods) {
    foreach ($methods as $method => $datafiles) {
      // Construct coverage class for the current class/method
      $filter = cover_simpletest_create_filter($class, $method);
      $methodcover = new PHP_CodeCoverage(NULL, $filter);
      $methodcover->setProcessUncoveredFilesFromWhitelist(TRUE);

      foreach ($datafiles as $path) {
        $data = unserialize(file_get_contents($path));
        if (is_array($data) && !empty($data)) {
          $methodcover->append($data, $class . '::' . $method);
        }
      }

      $allcover->merge($methodcover);
    }
  }

  $writer = new PHP_CodeCoverage_Report_HTML;
  $writer->process($allcover, $dest);

  drupal_goto(file_create_url($dest . '/index.html'));
}

/**
 * Return a nested array of paths to raw data files keyed by class name on the
 * first level and method name on the second.
 */
function cover_simpletest_scan_results($dir) {
  $results = array();

  // Scan directory structure for datafiles
  $classes = dir($dir);
  while (($class = $classes->read()) !== FALSE) {
    $path = $dir . '/' . $class;
    if ($class == '.' || $class == '..' || !is_dir($path)) {
      continue;
    }

    $methods = dir($path);
    while (($method = $methods->read()) !== FALSE) {
      $path = $dir . '/' . $class . '/' . $method;
      if ($method == '.' || $method == '..' || !is_dir($path)) {
        continue;
      }

      $datafiles = dir($path);
      while (($datafile = $datafiles->read()) !== FALSE) {
        $path = $dir . '/' . $class . '/' . $method . '/' . $datafile;
        if ($datafile == '.' || $datafile == '..' || !is_file($path)) {
          continue;
        }

        $results[str_replace('-', '\\', $class)][$method][] = $path;
      }
      $datafiles->close();
    }
    $methods->close();
  }
  $classes->close();

  return $results;
}

/**
 * Construct new PHP_CodeCoverage_Filter for the given test class and method
 */
function cover_simpletest_create_filter($class, $method) {
  $modulepaths = cover_simpletest_module_paths();

  $reflectionclass = new ReflectionClass($class);
  $path = $reflectionclass->getFileName();
  if (strpos($path, DRUPAL_ROOT) === 0) {
    $path = substr($path, strlen(DRUPAL_ROOT) + 1);
  }

  $filter = new PHP_CodeCoverage_Filter();

  $idx = cover_simpletest_module_find_enclosing($modulepaths, $path);
  if ($idx === FALSE) {
    $corepaths = cover_simpletest_core_paths();
    foreach ($corepaths as $path) {
      if (is_dir($path)) {
        $filter->addDirectoryToWhitelist($path, '.inc');
        $filter->addDirectoryToWhitelist($path, '.php');
      }
      else {
        $filter->addFileToWhitelist($path);
      }
    }
  }
  else {
    $moduleroot = $modulepaths[$idx];
    $filter->addDirectoryToWhitelist($moduleroot, '.inc');
    $filter->addDirectoryToWhitelist($moduleroot, '.install');
    $filter->addDirectoryToWhitelist($moduleroot, '.module');
    $filter->addDirectoryToWhitelist($moduleroot, '.php');
    $filter->addDirectoryToWhitelist($moduleroot, '.test');
  }

  drupal_alter('cover_simpletest_filter', $filter, $class, $method);

  return $filter;
}

/**
 * Return paths for core files outside the modules directory.
 */
function cover_simpletest_core_paths() {
  return array('includes', 'authorize.php', 'cron.php', 'index.php', 'install.php', 'update.php', 'xmlrpc.php');
}

/**
 * Return an ordered list of base paths for core and contrib modules.
 */
function cover_simpletest_module_paths() {
  $paths = &drupal_static(__FUNCTION__);

  if (isset($paths)) {
    return $paths;
  }

  $paths = array();
  foreach (drupal_system_listing('/\.module$/', 'modules', 'name', 0) as $name => $entry) {
    // Ignore test-helpers
    if (!preg_match('#_test$#', $name) && !preg_match('#/tests/#', $entry->uri) && !preg_match('#/simpletest/#', $entry->uri)) {
      $path = dirname($entry->uri);
      $paths[$path] = $path;
    }
  }
  sort($paths);
  $paths = array_reverse($paths);

  return $paths;
}

/**
 * Return the index of the module-path enclosing the given path.
 */
function cover_simpletest_module_find_enclosing($paths, $path) {
  foreach ($paths as $idx => $prefix) {
    if (strpos($path, $prefix) === 0) {
      return $idx;
    }
  }

  return FALSE;
}
